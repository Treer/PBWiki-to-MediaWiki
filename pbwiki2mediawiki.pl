#!/usr/bin/perl

# Originally obtained from https://www.knowledgelab.org.uk/Importing_pbwiki_to_mediawiki
# Updated at https://gitlab.com/Treer/PBWiki-to-MediaWiki

# pbwiki to media wiki conversion script
# Copyright (C) 2005 ekes -at- aktivix.org.uk 

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use strict;

my $pbwikiName = 'knowledgelab'; # if your wiki was located at oldplace.pbwiki.com, then put 'oldplace' here.
my $dir = '/tmp/knowledgelab';
my $exportfile = '/tmp/mediawiki_from_pbwiki.xml';
my $present = '2016-03-26T00:00:00Z';
my $user = 'pbwikiImport';
# !!! WARNING !!!
# Set this string to '' if you're not importing the pbwiki into a custom namespace in the
# MediaWiki (https://www.mediawiki.org/wiki/Manual:Using_custom_namespaces)
# Otherwise you'll get broken links when the wiki pages link to each other.
my $mediaWikiNamespace = 'PBWiki:'; # end this with a colon unless the string is empty (empty = no namespace)

my @pages;
my %revisions;

sub convertWiki {
	my ( $date, $page ) = @_;
	my ( $line, $text);

	my $indent = 0;

	open (WIKI, '<', $page);
	while ($line = <WIKI>) {
		$line =~ s/\\'/'/g;
		$line =~ s/\\"/"/g;
		$line =~ s/\r\n/\n/g; # Windows zipfile

		$line =~ s/&/&/g;
		$line =~ s/<CENTER>//gi;    # ARGH too many broken
		$line =~ s/<\/CENTER>//gi;  # unclosed etc for xml

		$line =~ s/<strong>/'''/gi;  
		$line =~ s/<\/strong>/'''/gi;  
		$line =~ s/<em>/''/g;
		$line =~ s/<\/em>/''/g;
		$line =~ s/<p>&nbsp;<\/p>//g;
		$line =~ s/<h[1-9]>&nbsp;<\/h[1-9]>//g;
		$line =~ s/<p><img class="pluginslug".*?<\/p>\n?//g;
		$line =~ s/&nbsp;/ /g;
		$line =~ s/<p.*?>//g;
		$line =~ s/<\/p>/\n/g;
		$line =~ s/<h1.*?>(.*)<\/h1>/=$1=/g;
		$line =~ s/<h2.*?>(.*)<\/h2>/==$1==/g;
		$line =~ s/<h3.*?>(.*)<\/h3>/===$1===/g;
		$line =~ s/<h4.*?>(.*)<\/h4>/====$1====/g;
		# escape & in urls
		while ($line =~ s/"http(.*?)&(?!amp;)(.*?)"/"http$1&amp;$2"/g) {}
		while ($line =~ s/\[http(.*?)&(?!amp;)(.*?) /\[http$1&amp;$2 /g) {}
		# remove + and - as spaces from wiki links
		while ($line =~ s/<a href="(?!http)(.*?)[+-](.*?)"/<a href="$1_$2"/g) {}

		$line =~ s/<!-- .*? -->\n//g; # the metadata gets filtered out during import leaving you with extra lines
		$line =~ s/<raw>/&lt;nowiki&gt;/g;
		$line =~ s/<\/raw>/&lt;\/nowiki&gt;/g;
		$line =~ s/&sup([0-9]);/&lt;sup&gt;$1&lt;\/sup&gt;/g;
		$line =~ s/&deg;/°/g;
		$line =~ s/<span.*?<\/span>//g; # wtf are pb_bookmarks?



		$line =~ s/<br>/&lt;br \/&gt;/g;

		$line =~ s/^!!!(.*)/===$1===/;
		$line =~ s/^!!(.*)/==$1==/;
		$line =~ s/^!(.*)/=$1=/;

		$line =~ s/^---/----/;

		$line =~ s/\*\*(.*)\*\*/'''$1'''/g;
		$line =~ s/__(.*)__/&lt;u&gt;$1&lt;\/u&gt;/g;
		# this substitution might be more accurate:
		# (I didn't want to change your script since I'm not 100% sure of my new regex -DaveBrondsema)
		# $line =~ s/(\s)-(\S.+)-([\s\$])/$1<s>$2<\/s>$3/g;
		$line =~ s/\s -(\S.*)- \s/&lt;strike&gt;$1&lt;\/strike&gt;/g;

		# these 2 substitutions might be more accurate more of the time (it's not completely perfect:
		# (I didn't want to change your script since I'm not 100% sure of my new regex -DaveBrondsema)
		# $line =~ s/\[([^:]*)\]/[[$1]]/g;
		# $line =~ s/\[(.*)\|(.*)\]/[$1 $2]/g;

		$line =~ s/\[(\w+:.*?)\]/[[$1]]/g;
		$line =~ s/\[\[(.*)\|(.*)\]\]/[[$1 $2]]/g;


		# We now assume double brackets, as single brackets in $line have been doubled by a replacement above.
		# external images that were actually to the local pbwiki
		$line =~ s/\[\[https?:\/\/$pbwikiName\.pbwiki\.com\/f\/(\S+?\.(jpg|png))\]\]/[[image:~~was uploaded to pbwiki~~\/$1]]/g;
		# external images
		$line =~ s/\[\[(https?:\/\/\S+?\.(jpg|png))\]\]/$1 /g;
		# internal images
		$line =~ s/\[\[(\S+?\.(jpg|png))\]\]/[[image:$1]]/g;



		$line =~ s/\s~([A-Z][a-z0-9_']+[A-Z][a-z0-9_']+)/ $1/g;
		# This will make links out of n-hump CamelCaps words
		$line =~ s/\s([A-Z][a-z0-9_']+([A-Z][a-z0-9_']+)+)/ [[$1]]/g;
		$line =~ s/\[\[http:\/\/$pbwikiName\.pbwiki\.com\/index.php\?wiki=(\S+)\s/\[\[$1\|/gi;


		# Handle nested unordered lists
		$indent += $line =~ s/<ul>//g;
		$line =~ s/<li>/'*' x $indent . ' '/ge;
		$line =~ s/<\/li>//g;
		$indent -= $line =~ s/<\/ul>//g;


		# fix html links
		$line =~ s/<a href="http(.*?)".*?>(.*?)<\/a>/[http$1 $2]/g;		
		$line =~ s/<a href="\/(.*?)".*?>(.*?)<\/a>/[[$mediaWikiNamespace$1|$2]]/g;		
		$line =~ s/<a href="(.*?)".*?>(.*?)<\/a>/[[$mediaWikiNamespace$1|$2]]/g;		
		$line =~ s/<a (.*?)>/&lt;a $1&gt;/g;
		$line =~ s/<\/a>/&lt;\/a&gt;/g;

		# external images that were actually to the local pbwiki
		$line =~ s/<img.*?src="https?:\/\/$pbwikiName\.pbwiki\.com\/f\/(.*?)".*?>/[[Image:\/~~was uploaded to pbwiki~~\/$1]] /g;
		$line =~ s/<img.*?src="f\/(.*?)".*?>/[[Image:\/~~was uploaded to pbwiki~~\/$1]] /g;

		# external images
		# TODO: let user specify the value of mediawiki's $wgAllowExternalImages and if it's
		#       false then put them in an [[Image ]]?
		#       (I have $wgAllowExternalImages set true, so the line below works well for me)
		$line =~ s/<img.*?src="(.*?)".*?>/$1 /g;

		# mark the files that are lost because pbwiki backup doesn't include them.
		$line =~ s/https?:\/\/$pbwikiName\.pbwiki\.com\/f\//\/~~was uploaded to pbwiki~~\//gi;
		# make local links local
		$line =~ s/https?:\/\/$pbwikiName\.pbwiki\.com\//\//gi;


		# Rather than figure out this script, I'm just going to try to clean
		# up external links with double brackets here
		$line =~ s/\[\[(http.*?)\]\]/[$1]/g;


		$text .= $line;
	}
	close WIKI;

	return "<revision>\n"
               ."<timestamp>$date</timestamp>\n"
	       ."<contributor><username>$user</username></contributor>\n"
	       ."<text>\n$text\n</text>"
	       ."</revision>\n";
}

opendir (DIR, $dir) or die "Can't open $dir: $!";
while (defined (my $file = readdir DIR)) {
	next if $file =~ /^\.\.?$/;

	my @page = ( $file =~ /([^.]+)\.?/g );
	$pages[++$#pages] = $page[0];# if @page == 1; # This if qualification was preventing the conversion, so I've commented it out as I don't know enough perl to figure out why it was put there.
	$revisions{$page[0]}[@{$revisions{$page[0]}}] = $page[1] if @page == 2;

}

open (XMLFILE, ">", $exportfile) or die "Can't open $exportfile to write: $!";
print XMLFILE "<mediawiki version=\"0.1\" xml:lang=\"en\">\n";

for my $page (@pages) {
	my $output;

	$output = "<page>\n"
                 ."<title>$page</title>\n";
	
	$output .= convertWiki( $present, "$dir/$page" );
	
	for my $revision (@{$revisions{$page}}) {
		my @t = ( $revision =~ /(\d+)-(\d+)-(\d+)-(\d+)-(\d+)/ );
		my $timestamp = $t[0].'-'.$t[1].'-'.$t[2].'T'.$t[3].':'.$t[4].':00Z';
		$output .= convertWiki( $timestamp, "$dir/$page.$revision" );
	}
	
	$output .= "</page>\n";
	print XMLFILE $output;
}

print XMLFILE "</mediawiki>\n";
close XMLFILE;

print "done\n";
