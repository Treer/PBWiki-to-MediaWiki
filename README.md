# PBWiki-to-MediaWiki

A perl script to convert the .zip backup files of wikis from pbwiki.com (defunct, now redirecting to [pbworks.com](http://www.pbworks.com/)) into an xml file that can be imported into MediaWiki wikis.

This script was originally written by [Ekes in 2005](https://www.knowledgelab.org.uk/Importing_pbwiki_to_mediawiki), and I've [updated it](https://gitlab.com/Treer/PBWiki-to-MediaWiki) to import my own wiki backup from 2015. 

I don't know if pbwiki's backup format has changed in that time, it doesn't seem to have a proper format &mdash; the pbwiki backup files are a mess and mix together different markup systems which presumably change depending on how each page was edited by the user. So perhaps this script might not work 100% in your wiki even though it works for my wiki.

## Usage

1. Unzip the pbwiki backup file into a directory somewhere.
2. Set the values at the top of `pbwiki2mediawiki.pl` to match your situation (i.e. `$dir`, `$exportfile`, `$pbwikiName`, `$mediaWikiNamespace`) 
3. Run the script. If you're using Windows you'll have to install perl in order to do this.
4. *[Optional]* If you want to import the pbwiki into a custom namespace, then [create the namespace](https://www.mediawiki.org/wiki/Manual:Using_custom_namespaces) and restart the web server.
5. Import the XML file using the Special Page "Import" (/mediawiki/index.php/Special:Import). This needs Wiki administrator privilege.
6. Check that all pages from the backup are listed as imported &mdash; no error is shown if Import fails midway through. If the last page is empty that's a good clue that import failed. Normally failure is due to the backup files contain formatting that `pbwiki2mediawiki.pl` doesn't know about, this will vary from wiki to wiki (since the pbwiki backups have no fixed format), proceed to the troubleshooting section...

## Troubleshooting

The MediaWiki import function requires strictly correct XML, so if MediaWiki is barfing on the XML file, you can get an idea of what's wrong with the file by

* Enabling MediaWiki's debug log ([set `$wgDebugLogFile`](https://www.mediawiki.org/wiki/Manual:How_to_debug#Logging)) and searching the log file for `: parser error :` &mdash; this will give you the line number in the XML file where the error occurred, and sometimes a description of the problem.
* If you can't access the debug log, you can often get a clue by running the XML file through an [XML validator](https://www.google.com.au/search?q=online+xml+validator+xsd) &mdash; you can find [the XSD file for that here](https://www.mediawiki.org/xml/export-0.10.xsd). This is less reliable than MediaWiki's debug log.
* Use MediaWiki's Export feature if you want to see how a markup is *supposed* to be represented in the XML file.

The pbwiki format is unpredictable, so sometimes bad XML slips through. You may have to get your hands dirty with perl, or fix up errors in the XML manually with a text editor.

## Other versions

Feeding my pbwiki backup into previous versions of this script produces an XML file riddled with errors. However, in case the backup format has changed, or in case my fixes don't work for everyone, and because everyone's wiki backup will be different, here are all the versions of this file that I know about:

* [Original script by Ekes, from November 2005](https://gitlab.com/Treer/PBWiki-to-MediaWiki/raw/fadf8ce364cf80ea8d8384ce06f7268ea5463fa2/pbwiki2mediawiki.pl)
* [Suggestions added by DaveBrondsema in April 2008](https://gitlab.com/Treer/PBWiki-to-MediaWiki/raw/fda73ef98eaa0d540d14c6e4529468a0779811ba/pbwiki2mediawiki.pl) - you will need to uncomment the suggestions if you want this file to behave differently to the original 2005 script.
* [My version, from March 2016](https://gitlab.com/Treer/PBWiki-to-MediaWiki/raw/b2c58e6997bacd8828b69addcba9742b3dc91c9f/pbwiki2mediawiki.pl)
